<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Room;

class RoomController extends Controller
{
    public function index($id = null) {
        if ($id) {
            return Room::find($id);
        }
        return Room::all();
    }

    public function create(Request $request) {
        Room::create(['name' => $request->name]);
        return 'Room created successfully';
    }

    public function update(Request $request) {
        $room = Room::find($request->room_id);

        if ($request->remove) {
            if ($room->player1 == $request->player_id) { 
                $room->update(['player1' => null]); 
            } else if ($room->player2 == $request->player_id) {
                $room->update(['player2' => null]); 
            }

            return 'Room updated successfully';
        }
        
        switch ($room) {
            case $room->player1 && $room->player2:
                return 'Room full';
            case !$room->player1 && $room->player2 != $request->player_id:
                $room->update(['player1' => $request->player_id]);
                break;
            case !$room->player2 && $room->player1 != $request->player_id:
                $room->update(['player2' => $request->player_id]);
                break;
        }

        return 'Room updated successfully';
    }

    public function delete(Request $request) {
        Room::find($request->id)->delete();
        return 'Room delete successfully';
    }
}
