<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function test($name = 'World') {
        return 'Hello '.$name.' !';
    }

    public function index($id = null) {
        if ($id) {
            return User::find($id);
        }
        return User::all();
    }

    public function update(Request $request) {
        $user = User::find($request->id);

        if ($request->win) {
            $user->update(['nbWin' => ($user->nbWin + 1), 'elo' => ($user->elo + 15)]);
        }

        if ($request->lose) {
            $user->update(['nbLose' => ($user->nbLose + 1), 'elo' => ($user->elo - 15)]);
        }

        return 'User updated successfully';
    }

    public function register(Request $request) {
        User::create(['pseudo' => $request->pseudo, 'password' => Hash::make($request->password)]);
        return 'User created successfully';
    }

    public function login(Request $request) {
        $user = User::firstWhere('pseudo', $request->pseudo);
        if (Hash::check($request->password, $user->password)) {
            return $user;
        }

        return response()->json(['error'=>'Unauthorised'], 401); 
    }
}
