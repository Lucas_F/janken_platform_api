<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'pseudo' => 'Soartiz',
            'password' => Hash::make('azerty')
        ]);
        DB::table('users')->insert([
            'pseudo' => 'Zitraos',
            'password' => Hash::make('azerty')
        ]);
        DB::table('users')->insert([
            'pseudo' => 'Volutix',
            'password' => Hash::make('azerty')
        ]);
    }
}
