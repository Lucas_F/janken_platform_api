<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test/{name?}', 'UserController@test');


Route::get('getUsers/{id?}', 'UserController@index');
Route::put('updateUser', 'UserController@update');
Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');

Route::get('getRooms/{id?}', 'RoomController@index');
Route::post('createRoom', 'RoomController@create');
Route::put('updateRoom', 'RoomController@update');
Route::post('deleteRoom', 'RoomController@delete');
